#!/bin/bash
if ! [ "${COMPOSE_DIR}" ]; then
	echo "FATAL: COMPOSE_DIR not set"
	exit 1
fi
BASE_DIR="$( cd "${COMPOSE_DIR}/.." > /dev/null && pwd )"

# Source relevant variables from .env file
eval "$(grep -E '^(DB_HOST|DB_PORT|DB_NAME|DB_USERNAME|DB_PASSWORD)=.+' "${COMPOSE_DIR}/clarin/.env")"

if ! [ "${DB_HOST}" ] || ! [ "${DB_PORT}" ] || ! [ "${DB_NAME}" ] || ! [ "${DB_USERNAME}" ] || ! [ "${DB_PASSWORD}" ]; then
	echo "FATAL: The following variables must be set in .env: DB_HOST DB_PORT DB_NAME DB_USERNAME DB_PASSWORD"
	exit 1
fi

if ! [ "${RESTORE_FILE}" ]; then
	if [ "$#" -ge 1 ]; then
		RESTORE_FILE="$1"
	else
		echo "FATAL: File to restore not specified"
		exit 1
	fi	
fi

echo "File to restore: ${RESTORE_FILE}"
echo "Resolving against ${BASE_DIR}"

RESTORE_FILE_ABSOLUTE="$(cd "${BASE_DIR}" > /dev/null && realpath "${RESTORE_FILE}" )"

echo "Absolute path of file to restore: ${RESTORE_FILE_ABSOLUTE}"

if [ -f "${RESTORE_FILE_ABSOLUTE}" ]; then
	#See https://www.postgresql.org/docs/9.3/libpq-pgpass.html
	PG_PASS_FILE="$( cd "${COMPOSE_DIR}" >/dev/null && pwd )/../.$(date +%Y%m%d%H%M%S)-${RANDOM}.pgpass.tmp"
	echo "${DB_HOST}:${DB_PORT}:${DB_NAME}:${DB_USERNAME}:${DB_PASSWORD}" > "${PG_PASS_FILE}"

	RESTORE_FILE_DIR="$(dirname "${RESTORE_FILE_ABSOLUTE}")"
	RESTORE_FILE_NAME="$(basename "${RESTORE_FILE_ABSOLUTE}")"
	(cd "${COMPOSE_DIR}/clarin" \
		&& COMPREG_BACKUP_RESTORE_PG_PASS="${PG_PASS_FILE}" BACKUPS_DIR="${RESTORE_FILE_DIR}" FILENAME="${RESTORE_FILE_NAME}" docker-compose \
		-f 'docker-compose.yml' \
		-f 'docker-compose-restore.yml' \
		up --force-recreate restore)
	rm "${PG_PASS_FILE}"
else
	echo "FATAL: Backup file ${RESTORE_FILE} not found in ${BASE_DIR}"
	exit 1
fi
