#!/bin/bash

# Source relevant variables from .env file
eval "$(grep -E '^(DB_HOST|DB_PORT|DB_NAME|DB_USERNAME|DB_PASSWORD)=.+' "${COMPOSE_DIR}/clarin/.env")"

if ! [ "${DB_HOST}" ] || ! [ "${DB_PORT}" ] || ! [ "${DB_NAME}" ] || ! [ "${DB_USERNAME}" ] || ! [ "${DB_PASSWORD}" ]; then
	echo "FATAL: The following variables must be set in .env: DB_HOST DB_PORT DB_NAME DB_USERNAME DB_PASSWORD"
	exit 1
fi

#See https://www.postgresql.org/docs/9.3/libpq-pgpass.html
PG_PASS_FILE="$( cd "${COMPOSE_DIR}" >/dev/null && pwd )/../.$(date +%Y%m%d%H%M%S)-${RANDOM}.pgpass.tmp"
echo "${DB_HOST}:${DB_PORT}:${DB_NAME}:${DB_USERNAME}:${DB_PASSWORD}" > "${PG_PASS_FILE}"

OLD_PWD="$(pwd)"
cd "${COMPOSE_DIR}/clarin" || exit 1

if COMPREG_BACKUP_RESTORE_PG_PASS="${PG_PASS_FILE}" TIMESTAMP="${BACKUPS_TIMESTAMP}" docker-compose \
		-f 'docker-compose.yml' \
		-f 'docker-compose-backup.yml' \
		run --rm backup; then
	rm "${PG_PASS_FILE}"
	cd "${OLD_PWD}" || exit 1
	echo "Backup completed"
else
	rm "${PG_PASS_FILE}"
	cd "${OLD_PWD}" || exit 1
	echo "ERROR: Backup FAILED"
	exit 1
fi
