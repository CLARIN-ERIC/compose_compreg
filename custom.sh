#!/bin/bash

set -e

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# shellcheck source=./common-lib.sh
source "${BASE_DIR}/common-lib.sh"

DEFAULT_VERSION="020200"
SERVICE_NAME="database"

VERSION_TABLE="_app_db_version"
CREATE_TABLE_SQL="CREATE TABLE IF NOT EXISTS ${VERSION_TABLE}(version INTEGER, updated TIMESTAMP);"
SELECT_VERSION_SQL="SELECT version FROM ${VERSION_TABLE} ORDER BY updated DESC LIMIT 1;"

UPDATE_SCRIPT_URL_v020300="https://raw.githubusercontent.com/clarin-eric/component-registry-rest/2.3.1/ComponentRegistry/src/main/sql/upgrade-2.3.sql"
UPDATE_SCRIPT_URL_v020400="https://raw.githubusercontent.com/clarin-eric/component-registry-rest/2.4.0/ComponentRegistry/src/main/sql/upgrade-2.4.sql"

if [ -e "${COMPOSE_DIR}/.env" ]; then
	COMPOSE_CLARIN_DIR="${COMPOSE_DIR}"
else
	COMPOSE_CLARIN_DIR="${COMPOSE_DIR}/clarin"
fi

if ! [ -e "${COMPOSE_CLARIN_DIR}/.env" ]; then
	error "Not a valid compose project directory: ${COMPOSE_CLARIN_DIR} (.env file not found)"
	exit 1
fi	

main() {
	# Process subcommands
	#
	subcommand=$1
	case $subcommand in
		"" | "-h" | "--help")
			sub_help
			;;
		"apply-update" )
			apply_update
			;;
		*)
			shift
			"sub_${subcommand}" "$@"
			if [ $? = 127 ]; then
				echo "Error: '${subcommand}' is not a known subcommand." >&2
				echo "       Run '${PROGRAM_NAME} --help' for a list of known subcommands." >&2
				exit 1
			fi
			;;
	esac
}

sub_help(){
    echo "Usage: ${PROGRAM_NAME} <subcommand> [options]"
    echo ""
    echo "Subcommands:"
    echo "    apply-update                Apply (database) updates to ensure compatibility with latest version"
    echo ""
    echo "For help with each subcommand run:"
    echo "${PROGRAM_NAME} <subcommand> -h|--help"
    echo ""
}

apply_update() {	
	(
		cd "${COMPOSE_CLARIN_DIR}" || (echo "Failed to move to directory: ${COMPOSE_CLARIN_DIR}"; exit 1)
		
		echo -n "Checking whether container is running: "
		docker-compose up -d "${SERVICE_NAME}"

		wait_for_db
		
		echo "Checking for version in database..."
		_detect_version
		APP_VERSION="${DETECTED_VERSION}"
		echo -e "\nVersion: [${APP_VERSION}]\n"
		
		if ! [ "${APP_VERSION}" ]; then
			echo "FATAL: App version could not be determined"
			exit 1
		fi
		
		_upgrade_to_version_with_script "${APP_VERSION}" "020300" "2.3.0" "${UPDATE_SCRIPT_URL_v020300}"
		_upgrade_to_version_with_script "${APP_VERSION}" "020400" "2.4.0" "${UPDATE_SCRIPT_URL_v020400}"
		
		echo -e "\nDone!"
	)
	
	exit 0
}

_upgrade_to_version_with_script() {
	CURRENT_VERSION_CODE="$1"
	VERSION_CODE="$2"
	VERSION_NAME="$3"
	VERSION_SCRIPT_URL="$4"
	
	if [ "${CURRENT_VERSION_CODE}" -ge "${VERSION_CODE}" ]; then
		echo "Already at >= ${VERSION_NAME}"
	else
		echo -e "---- Upgrading Component Registry database schema to version ${VERSION_NAME}... ----\n"
		if _upgrade_with_script "${VERSION_SCRIPT_URL}"; then
			_add_version "${VERSION_CODE}"
		else
			_detect_version > /dev/null 2>&1
			echo "Version NOT updated in database. Remaining at ${DETECTED_VERSION}."
			exit 1
		fi
	fi
}

_upgrade_with_script() {
	SCRIPT_URL="$1"
	echo "Fetching script from ${SCRIPT_URL}..."
	
	SCRIPT_TMP_FILE="$(mktemp)"
	if ! curl -sSf "${SCRIPT_URL}" > "$SCRIPT_TMP_FILE"; then
		echo -e "\nFATAL: failed to retrieve script!"
		return 1
	fi
	
	echo "Applying update script to running database instance..."
	
	if ! _exec_psql_in_db_container < "${SCRIPT_TMP_FILE}"; then
		echo -e "\nFATAL: failed to apply upgrade script!"
		rm "${SCRIPT_TMP_FILE}"
		return 1
	fi
	
	rm "${SCRIPT_TMP_FILE}"
}

wait_for_db() {
	_exec_script_in_container "${SERVICE_NAME}" "if pg_isready -q; then echo 'Database is ready!'; else echo -n 'Waiting for database'; while ! pg_isready -q ; do echo -n '.'; sleep 1; done; echo ' ready!'; fi"
}

_exec_script_in_container() {
	CONTAINER="$1"
	shift
	docker-compose exec -T "${CONTAINER}" bash -c "$*"
}

_exec_psql_in_db_container() {
	_exec_script_in_container "${SERVICE_NAME}" "psql -U \$POSTGRES_USER -d \$POSTGRES_DB $*"
}

_exec_psql_cmd_in_db_container() {
	_exec_psql_in_db_container "-t -A -F\",\" -c \"$*\""
}

_add_version() {
	NEW_VERSION="$1"
	echo "Setting version to [${NEW_VERSION}] in database..."
	_exec_psql_cmd_in_db_container "INSERT INTO ${VERSION_TABLE}(version, updated) VALUES('${NEW_VERSION}', NOW());" >&2
}

_detect_version() {
	# make sure versions table exists in database
	_exec_psql_cmd_in_db_container "${CREATE_TABLE_SQL}" >&2
	
	# read version from database
	VERSION_FROM_DB="$(_exec_psql_cmd_in_db_container "${SELECT_VERSION_SQL}")" >&2
	if [ "${VERSION_FROM_DB}" = "" ]; then
		# set default version
		echo "WARNING: No version set in database. Setting default!" >&2
		_add_version "${DEFAULT_VERSION}" >&2
		# read again
		VERSION_FROM_DB="$(_exec_psql_cmd_in_db_container "${SELECT_VERSION_SQL}")" >&2
	fi
	DETECTED_VERSION="${VERSION_FROM_DB}"
}

main "$@"

