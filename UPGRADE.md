# Upgrade instructions

## 2.4.3 >= 2.4.4

1. Tomcat has been upgraded from 8.5.x to 9.0.x. `CATALINA_HOME` has changed from 
`/srv/tomcat8` to `/srv/tomcat`. Make sure that any hard references to the former path
in the configuration (`.env` and/or any custom overlays) have been updated or factored
out.

2. The Postgres version has been upgraded from 13.x to 15.x. The data directories of these 
versions are not compatible. Therefore, when upgrading, make sure to follow these steps
in sequence:

### BEFORE bringing down the old version or deploying the new version of the Component Registry

```
HOME_DEPLOY="/home/deploy"
PROJ_NAME="compreg"
if [ "${PROJ_NAME}" ] && [ -d "${HOME_DEPLOY}/${PROJ_NAME}" ]; then
	(cd "${HOME_DEPLOY}" \
		&& ./control.sh "${PROJ_NAME}" backup \
		&& ./control.sh "${PROJ_NAME}" stop) && \
	(cd "${HOME_DEPLOY}/${PROJ_NAME}/compose_compreg/clarin" \
		&& docker-compose down -v --remove-orphans)
fi
```

__Now deploy and start the new version according to standard procedure.__

_Wait_ until the `database` service is UP and HEALTHY (it can happen that other
services will NOT reach healthy state until a database restore has taken place, in which
case this can be ignored for the moment.)

### AFTER deploying the new version of the Component Registry

```
if [ "${PROJ_NAME}" ] && [ -d "${HOME_DEPLOY}/${PROJ_NAME}" ]; then
	(cd "${HOME_DEPLOY}" \
		&& ./control.sh "${PROJ_NAME}" restore latest)
fi
```

Check that __ALL__ services come __UP__ and reach a __HEALTHY__ state!


## 2.4.2 >= 2.4.3
The Postgres version has been upgraded from 11.x to 13.x. The data directories of these 
versions are not compatible. Therefore, when upgrading, make sure to follow these steps
in sequence:

### BEFORE bringing down the old version or deploying the new version of the Component Registry

```
HOME_DEPLOY="/home/deploy"
PROJ_NAME="compreg"
if [ "${PROJ_NAME}" ] && [ -d "${HOME_DEPLOY}/${PROJ_NAME}" ]; then
	(cd "${HOME_DEPLOY}" \
		&& ./control.sh "${PROJ_NAME}" backup \
		&& ./control.sh "${PROJ_NAME}" stop) && \
	(cd "${HOME_DEPLOY}/${PROJ_NAME}/compose_compreg/clarin" \
		&& docker-compose down -v --remove-orphans)
fi
```

Now deploy and start the new version according to standard procedure. 

Wait until the `database` service is UP and HEALTHY (it can be expected that other
services will NOT reach healthy state until a database restore has taken place!)

### AFTER deploying the new version of the Component Registry

```
if [ "${PROJ_NAME}" ] && [ -d "${HOME_DEPLOY}/${PROJ_NAME}" ]; then
	(cd "${HOME_DEPLOY}" \
		&& ./control.sh "${PROJ_NAME}" restore latest \
		&& ./control.sh "${PROJ_NAME}" restart)
fi
```

Check that ALL services come up and reach a healthy state!

## 2.4.1 >= 2.4.2
No configuration changes needed

## 2.4.0 >= 2.4.1
No configuration changes needed

## 2.3.x >= 2.4.0

An update to the database schema and content is required for this release. The
[script](https://github.com/clarin-eric/component-registry-rest/blob/2.4.0/ComponentRegistry/src/main/sql/upgrade-2.4.sql)
can be executed using the `apply-update` command via the control script.

## 2.3.x >= 2.3.1c

The Postgres version has been upgraded from 9.x to 11.x. The data directories of these 
versions are not compatible with this version 11.7. Therefore, when upgrading, make
sure to follow these steps in sequence:

### BEFORE bringing down the old version or deploying the new version of the Component Registry

```
HOME_DEPLOY="/home/deploy"
PROJ_NAME="compreg"
if [ "${PROJ_NAME}" ] && [ -d "${HOME_DEPLOY}/${PROJ_NAME}" ]; then
	(cd "${HOME_DEPLOY}" \
		&& ./control.sh "${PROJ_NAME}" backup \
		&& ./control.sh "${PROJ_NAME}" stop) && \
	(cd "${HOME_DEPLOY}/${PROJ_NAME}/compose_compreg/clarin" \
		&& docker-compose down -v --remove-orphans)
fi
```

Now deploy and start the new version according to standard procedure. 

Wait until the database service is up and healthy (it can be expected that other services
will NOT reach healthy state until a database restore has taken place!)

### AFTER deploying the new version of the Component Registry

```
if [ "${PROJ_NAME}" ] && [ -d "${HOME_DEPLOY}/${PROJ_NAME}" ]; then
	(cd "${HOME_DEPLOY}" \
		&& ./control.sh "${PROJ_NAME}" restore latest \
		&& ./control.sh "${PROJ_NAME}" restart)
fi
```

Check that ALL services come up and reach a healthy state!
