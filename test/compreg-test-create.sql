--
-- PostgreSQL database dump
--

-- Dumped from database version 11.10
-- Dumped by pg_dump version 11.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: _app_db_version; Type: TABLE; Schema: public; Owner: compreg
--

CREATE TABLE public._app_db_version (
    version integer,
    updated timestamp without time zone
);


ALTER TABLE public._app_db_version OWNER TO compreg;

--
-- Name: basedescription; Type: TABLE; Schema: public; Owner: compreg
--

CREATE TABLE public.basedescription (
    id integer NOT NULL,
    user_id integer,
    is_public boolean NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    component_id character varying NOT NULL,
    name character varying NOT NULL,
    description character varying NOT NULL,
    registration_date timestamp with time zone,
    creator_name character varying,
    domain_name character varying,
    group_name character varying,
    href character varying,
    show_in_editor boolean DEFAULT true NOT NULL,
    content text DEFAULT ''::text NOT NULL,
    status integer,
    derivedfrom character varying,
    successor character varying,
    recommended boolean DEFAULT false NOT NULL
);


ALTER TABLE public.basedescription OWNER TO compreg;

--
-- Name: basedescription_id_seq; Type: SEQUENCE; Schema: public; Owner: compreg
--

CREATE SEQUENCE public.basedescription_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.basedescription_id_seq OWNER TO compreg;

--
-- Name: basedescription_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: compreg
--

ALTER SEQUENCE public.basedescription_id_seq OWNED BY public.basedescription.id;


--
-- Name: comments; Type: TABLE; Schema: public; Owner: compreg
--

CREATE TABLE public.comments (
    id integer NOT NULL,
    comments text NOT NULL,
    comment_date timestamp with time zone NOT NULL,
    component_id character varying,
    user_id integer NOT NULL,
    user_name character varying
);


ALTER TABLE public.comments OWNER TO compreg;

--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: public; Owner: compreg
--

CREATE SEQUENCE public.comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comments_id_seq OWNER TO compreg;

--
-- Name: comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: compreg
--

ALTER SEQUENCE public.comments_id_seq OWNED BY public.comments.id;


--
-- Name: groupmembership; Type: TABLE; Schema: public; Owner: compreg
--

CREATE TABLE public.groupmembership (
    id integer NOT NULL,
    groupid integer,
    userid integer
);


ALTER TABLE public.groupmembership OWNER TO compreg;

--
-- Name: groupmembership_id_seq; Type: SEQUENCE; Schema: public; Owner: compreg
--

CREATE SEQUENCE public.groupmembership_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groupmembership_id_seq OWNER TO compreg;

--
-- Name: groupmembership_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: compreg
--

ALTER SEQUENCE public.groupmembership_id_seq OWNED BY public.groupmembership.id;


--
-- Name: itemlock_id_seq; Type: SEQUENCE; Schema: public; Owner: compreg
--

CREATE SEQUENCE public.itemlock_id_seq
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.itemlock_id_seq OWNER TO compreg;

--
-- Name: itemlock; Type: TABLE; Schema: public; Owner: compreg
--

CREATE TABLE public.itemlock (
    id integer DEFAULT nextval('public.itemlock_id_seq'::regclass) NOT NULL,
    itemid integer NOT NULL,
    userid integer NOT NULL,
    creationdate timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.itemlock OWNER TO compreg;

--
-- Name: ownership; Type: TABLE; Schema: public; Owner: compreg
--

CREATE TABLE public.ownership (
    id integer NOT NULL,
    componentid character varying(255),
    groupid integer,
    userid integer
);


ALTER TABLE public.ownership OWNER TO compreg;

--
-- Name: ownership_id_seq; Type: SEQUENCE; Schema: public; Owner: compreg
--

CREATE SEQUENCE public.ownership_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ownership_id_seq OWNER TO compreg;

--
-- Name: ownership_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: compreg
--

ALTER SEQUENCE public.ownership_id_seq OWNED BY public.ownership.id;


--
-- Name: registry_user; Type: TABLE; Schema: public; Owner: compreg
--

CREATE TABLE public.registry_user (
    id integer NOT NULL,
    name character varying,
    principal_name character varying
);


ALTER TABLE public.registry_user OWNER TO compreg;

--
-- Name: registry_user_id_seq; Type: SEQUENCE; Schema: public; Owner: compreg
--

CREATE SEQUENCE public.registry_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.registry_user_id_seq OWNER TO compreg;

--
-- Name: registry_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: compreg
--

ALTER SEQUENCE public.registry_user_id_seq OWNED BY public.registry_user.id;


--
-- Name: usergroup; Type: TABLE; Schema: public; Owner: compreg
--

CREATE TABLE public.usergroup (
    id integer NOT NULL,
    ownerid integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.usergroup OWNER TO compreg;

--
-- Name: usergroup_id_seq; Type: SEQUENCE; Schema: public; Owner: compreg
--

CREATE SEQUENCE public.usergroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usergroup_id_seq OWNER TO compreg;

--
-- Name: usergroup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: compreg
--

ALTER SEQUENCE public.usergroup_id_seq OWNED BY public.usergroup.id;


--
-- Name: basedescription id; Type: DEFAULT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.basedescription ALTER COLUMN id SET DEFAULT nextval('public.basedescription_id_seq'::regclass);


--
-- Name: comments id; Type: DEFAULT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.comments ALTER COLUMN id SET DEFAULT nextval('public.comments_id_seq'::regclass);


--
-- Name: groupmembership id; Type: DEFAULT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.groupmembership ALTER COLUMN id SET DEFAULT nextval('public.groupmembership_id_seq'::regclass);


--
-- Name: ownership id; Type: DEFAULT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.ownership ALTER COLUMN id SET DEFAULT nextval('public.ownership_id_seq'::regclass);


--
-- Name: registry_user id; Type: DEFAULT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.registry_user ALTER COLUMN id SET DEFAULT nextval('public.registry_user_id_seq'::regclass);


--
-- Name: usergroup id; Type: DEFAULT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.usergroup ALTER COLUMN id SET DEFAULT nextval('public.usergroup_id_seq'::regclass);


--
-- Data for Name: _app_db_version; Type: TABLE DATA; Schema: public; Owner: compreg
--

COPY public._app_db_version (version, updated) FROM stdin;
20200	2019-12-02 12:34:29.888992
20300	2019-12-02 12:34:32.788969
20400	2021-01-12 10:22:55.432197
\.



--
-- Name: basedescription_id_seq; Type: SEQUENCE SET; Schema: public; Owner: compreg
--

SELECT pg_catalog.setval('public.basedescription_id_seq', 4710, true);


--
-- Name: comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: compreg
--

SELECT pg_catalog.setval('public.comments_id_seq', 115, true);


--
-- Name: groupmembership_id_seq; Type: SEQUENCE SET; Schema: public; Owner: compreg
--

SELECT pg_catalog.setval('public.groupmembership_id_seq', 108, true);


--
-- Name: itemlock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: compreg
--

SELECT pg_catalog.setval('public.itemlock_id_seq', 2, false);


--
-- Name: ownership_id_seq; Type: SEQUENCE SET; Schema: public; Owner: compreg
--

SELECT pg_catalog.setval('public.ownership_id_seq', 618, true);


--
-- Name: registry_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: compreg
--

SELECT pg_catalog.setval('public.registry_user_id_seq', 421, true);


--
-- Name: usergroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: compreg
--

SELECT pg_catalog.setval('public.usergroup_id_seq', 20, true);


--
-- Name: basedescription basedescription_pkey; Type: CONSTRAINT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.basedescription
    ADD CONSTRAINT basedescription_pkey PRIMARY KEY (id);


--
-- Name: comments comments_id_pkey; Type: CONSTRAINT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_id_pkey PRIMARY KEY (id);


--
-- Name: basedescription constraint_basedescription_unique_id; Type: CONSTRAINT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.basedescription
    ADD CONSTRAINT constraint_basedescription_unique_id UNIQUE (id);


--
-- Name: groupmembership groupmembership_pkey; Type: CONSTRAINT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.groupmembership
    ADD CONSTRAINT groupmembership_pkey PRIMARY KEY (id);


--
-- Name: itemlock itemlock_pkey; Type: CONSTRAINT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.itemlock
    ADD CONSTRAINT itemlock_pkey PRIMARY KEY (id);


--
-- Name: itemlock lock; Type: CONSTRAINT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.itemlock
    ADD CONSTRAINT lock UNIQUE (itemid);


--
-- Name: ownership ownership_pkey; Type: CONSTRAINT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.ownership
    ADD CONSTRAINT ownership_pkey PRIMARY KEY (id);


--
-- Name: registry_user registry_user_principal_name_key; Type: CONSTRAINT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.registry_user
    ADD CONSTRAINT registry_user_principal_name_key UNIQUE (principal_name);


--
-- Name: registry_user user_pkey; Type: CONSTRAINT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.registry_user
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: usergroup usergroup_pkey; Type: CONSTRAINT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.usergroup
    ADD CONSTRAINT usergroup_pkey PRIMARY KEY (id);


--
-- Name: fki_comments_component_id; Type: INDEX; Schema: public; Owner: compreg
--

CREATE INDEX fki_comments_component_id ON public.comments USING btree (component_id);


--
-- Name: fki_comments_fk_user; Type: INDEX; Schema: public; Owner: compreg
--

CREATE INDEX fki_comments_fk_user ON public.comments USING btree (user_id);


--
-- Name: idx_basedescription_id; Type: INDEX; Schema: public; Owner: compreg
--

CREATE INDEX idx_basedescription_id ON public.basedescription USING btree (id);


--
-- Name: idx_basedescription_recommended; Type: INDEX; Schema: public; Owner: compreg
--

CREATE INDEX idx_basedescription_recommended ON public.basedescription USING btree (recommended);


--
-- Name: idx_basedescription_status; Type: INDEX; Schema: public; Owner: compreg
--

CREATE INDEX idx_basedescription_status ON public.basedescription USING btree (status);


--
-- Name: idx_component_id; Type: INDEX; Schema: public; Owner: compreg
--

CREATE INDEX idx_component_id ON public.basedescription USING btree (component_id);


--
-- Name: idx_is_deleted; Type: INDEX; Schema: public; Owner: compreg
--

CREATE INDEX idx_is_deleted ON public.basedescription USING btree (is_deleted);


--
-- Name: idx_is_public; Type: INDEX; Schema: public; Owner: compreg
--

CREATE INDEX idx_is_public ON public.basedescription USING btree (is_public);


--
-- Name: idx_user_id; Type: INDEX; Schema: public; Owner: compreg
--

CREATE INDEX idx_user_id ON public.basedescription USING btree (user_id);


--
-- Name: comments comments_user; Type: FK CONSTRAINT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_user FOREIGN KEY (user_id) REFERENCES public.registry_user(id);


--
-- Name: basedescription fk_basedescription_user_id; Type: FK CONSTRAINT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.basedescription
    ADD CONSTRAINT fk_basedescription_user_id FOREIGN KEY (user_id) REFERENCES public.registry_user(id) MATCH FULL;


--
-- Name: itemlock itemid; Type: FK CONSTRAINT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.itemlock
    ADD CONSTRAINT itemid FOREIGN KEY (itemid) REFERENCES public.basedescription(id);


--
-- Name: itemlock userid; Type: FK CONSTRAINT; Schema: public; Owner: compreg
--

ALTER TABLE ONLY public.itemlock
    ADD CONSTRAINT userid FOREIGN KEY (userid) REFERENCES public.registry_user(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: compreg
--

GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: TABLE basedescription; Type: ACL; Schema: public; Owner: compreg
--

GRANT ALL ON TABLE public.basedescription TO postgres;


--
-- Name: SEQUENCE basedescription_id_seq; Type: ACL; Schema: public; Owner: compreg
--

GRANT ALL ON SEQUENCE public.basedescription_id_seq TO postgres;


--
-- Name: TABLE comments; Type: ACL; Schema: public; Owner: compreg
--

GRANT ALL ON TABLE public.comments TO postgres;


--
-- Name: SEQUENCE comments_id_seq; Type: ACL; Schema: public; Owner: compreg
--

GRANT ALL ON SEQUENCE public.comments_id_seq TO postgres;


--
-- Name: TABLE groupmembership; Type: ACL; Schema: public; Owner: compreg
--

GRANT ALL ON TABLE public.groupmembership TO postgres;


--
-- Name: SEQUENCE groupmembership_id_seq; Type: ACL; Schema: public; Owner: compreg
--

GRANT ALL ON SEQUENCE public.groupmembership_id_seq TO postgres;


--
-- Name: TABLE ownership; Type: ACL; Schema: public; Owner: compreg
--

GRANT ALL ON TABLE public.ownership TO postgres;


--
-- Name: SEQUENCE ownership_id_seq; Type: ACL; Schema: public; Owner: compreg
--

GRANT ALL ON SEQUENCE public.ownership_id_seq TO postgres;


--
-- Name: TABLE registry_user; Type: ACL; Schema: public; Owner: compreg
--

GRANT ALL ON TABLE public.registry_user TO postgres;


--
-- Name: SEQUENCE registry_user_id_seq; Type: ACL; Schema: public; Owner: compreg
--

GRANT ALL ON SEQUENCE public.registry_user_id_seq TO postgres;


--
-- Name: TABLE usergroup; Type: ACL; Schema: public; Owner: compreg
--

GRANT ALL ON TABLE public.usergroup TO postgres;


--
-- Name: SEQUENCE usergroup_id_seq; Type: ACL; Schema: public; Owner: compreg
--

GRANT ALL ON SEQUENCE public.usergroup_id_seq TO postgres;


--
-- PostgreSQL database dump complete
--

